class Student

	attr_accessor :name, :age, :program, :major

	def initialize(name,age,program,major)
		@name = name
		@age = age
		@program = program
		@major = major
	end

end


def put_student(student_info)
	puts "(#{student_info.name}, #{student_info.age}, #{student_info.program}, #{student_info.major})"
end

def sort_students(student_array, symbol)

	for i in 0..student_array.length-1
		for j in (i + 1)..student_array.length-1
			if student_array[i].send(symbol) > student_array[j].send(symbol) then
				mid_student = student_array[i]
				student_array[i] = student_array[j]
				student_array[j] = mid_student
			end
		end
	end
end
Student1 = Student.new("john", 21, "MA", "IT")
Student2 = Student.new("Peter", 23, "PHD", "Ecology")
Student3 = Student.new("Ablaham", 22, "BA", "Design")
Student4 = Student.new("Noel", 20, "MDIV", "Theology")

student_array = [Student1, Student2, Student3, Student4]


sort_students(student_array, :age)

student_array.each do |student|
  put_student(student)
end



