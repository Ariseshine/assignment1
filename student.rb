class Student
	attr_reader :name, :age, :program, :major

	def constructor(new_name, new_age, new_program, new_major)
    	@name = new_name
    	@age = new_age
    	@program = new_program
    	@major = new_major
    end

	def display
		puts '/#' + '{' + "#{name}" + '}' + '-' + '#' + '{' + "#{age}" + '}' + '-' + '#' + '{' + "#{program}" + '}' + '-' +	'#' + '{' + "#{major}" + '}'
	end
end

Stu = Student.new

Stu.constructor "John", 18, "BA", "IT"
Stu.display

